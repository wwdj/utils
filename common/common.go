package common

func MapReduce(
	eden interface{},
	list []interface{},
	to func(interface{}) interface{},
	count func(interface{}, interface{}) interface{},
	end func(interface{})) {

	size := len(list)
	for i := 0; i < size; i++ {
		item := list[i]
		r := to(item)
		eden = count(eden, r)
	}
	end(eden)
}
