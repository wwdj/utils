package main

import (
	"wdj_utils/net_utils"
	"log"
	"encoding/json"
	"time"
	"database/sql"
	"wdj_utils/db_utils"
)

var addStmt *sql.Stmt

func init() {
	addSql := "INSERT INTO `db_news`.`spider_discover_data`( `info_type`,`info_source`, `info_source_type`, `info_source_channel`, `publisher`, `author`, `classification`, `keyword`, `language`, `city`, `info_publish_time`, `first_time`, `recent_time`, `update_time`, `crawl_mode`, `title`, `abstracts`, `content`, `video_duration`, `copyright`, `is_copyright`, `cover_imgs_num`, `cover_imgs`, `detail_imgs_num`, `detail_imgs`, `info_source_score`, `publisher_score`, `is_comment`, `jump_mode`, `display_mode`, `state`, `info_source_id`, `is_upload_img`, `updated_at`, `level`, `cp_flag`, `publish_mode`, `cover_images`, `is_recommend`) VALUES ( '1',  '小葱', 'web', '快讯', '小葱', NULL, NULL, NULL, '中文', NULL, FROM_UNIXTIME(?), NOW(), NULL, NULL, 0, ?, ?, ?, NULL, NULL, 1, 0, '[]', NULL, NULL, -1, -1, 1, NULL, NULL, 1, ?, 0, NOW(), -1, NULL, -1, NULL, 0);"
	stmt, err := db_utils.Db.Prepare(addSql)
	if err != nil {
		log.Fatalln(err)
	}
	addStmt = stmt
}

func main() {
	type XiaoChongItem struct {
		Id           int64    `json:"id"`
		Content      string   `json:"content"`
		ContentMore  string   `json:"content_more"`
		ContentText  string   `json:"content_text"`
		ContentTitle string   `json:"content_title"`
		DisplayTime  int64    `json:"display_time"`
		ImageUris    []string `json:"image_uris"`
		Score        int      `json:"score"`
		UpdatedAt    int64    `json:"updated_at"`
	}
	type XiaoChongData struct {
		Items      []XiaoChongItem `json:"Items"`
		NextCursor string          `json:"next_cursor"`
	}
	type XiaoChongRsp struct {
		Code int           `json:"code"`
		Data XiaoChongData `json:"data"`
	}

	meta := map[string]interface{}{
		"coop_id":  "teleboxio",
		"coop_key": "rd42e77s581xrg5dz62az0g4c29e11ez",
		"channel":  "xiaocong-channel",
		"score":    "1,2,3",
		"limit":    100,
	}

	var xcData = new(XiaoChongRsp)
	ticket := time.NewTicker(30 * time.Second)
	maxId := xcId()
	for {
		rsp, err := net_utils.HttpGet("http://cong-api.xcong.com/apiv1/content/livenews", meta, nil)
		if err != nil {
			log.Println(err.Error())
		} else if err = json.Unmarshal([]byte(rsp.BodyStr), xcData); err != nil {
			log.Println(err.Error())
		} else if xcData.Code != 20000 {
			log.Println("code != 2000 data:" + rsp.BodyStr)
		} else {
			size := len(xcData.Data.Items)
			for i := range xcData.Data.Items {
				item := xcData.Data.Items[size-1-i]
				if item.Id <= maxId {
					continue
				}
				maxId = item.Id
				_, err := addStmt.Exec(item.DisplayTime, item.ContentTitle, item.ContentTitle, item.ContentText, item.Id)
				if err != nil {
					log.Println("sql error:" + err.Error())
				} else {
					log.Println("insert ok!")
				}
			}
			log.Println("call ok!")
		}
		<-ticket.C
	}
}

func xcId() int64 {
	rs := db_utils.Db.QueryRow("select info_source_id from spider_discover_data where info_source='小葱' order by id desc limit 1")
	var id int64
	rs.Scan(&id)
	return id
}
