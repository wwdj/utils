package net_utils

import (
	"encoding/json"
	"strings"
	"net/http"
	"io/ioutil"
	"bytes"
)
var client = &http.Client{}

func HttpGet(urlLine string, ps map[string]interface{}, header map[string]string) (*WDJResponse, error) {
	urlLine += "?" + toParamsContent(ps)
	rq,err := http.NewRequest("GET", urlLine, nil)
	if err != nil {
		return nil, err
	}
	rq.Close = true
	for k, v := range header {
		rq.Header.Add(k, v)
	}
	rsp, err := client.Do(rq)
	if err != nil {
		return nil, err
	}
	bs, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return nil, err
	}
	return &WDJResponse{rsp, string(bs)}, nil
}

func HttpPost(urlLine string, ps map[string]interface{}, header map[string]string) (*WDJResponse, error) {
	content := toParamsContent(ps)
	body := strings.NewReader(content)
	rq,err := http.NewRequest("POST", urlLine, body)
	if err != nil {
		return nil, err
	}
	rq.Close = true
	rq.Header.Add("Content-Type","application/x-www-form-urlencoded")
	for k, v := range header {
		rq.Header.Add(k, v)
	}
	rsp, err := client.Do(rq)
	if err != nil {
		return nil, err
	}
	bs, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return nil, err
	}
	return &WDJResponse{rsp, string(bs)}, nil
}

func HttpPostJson(urlLine string, obj interface{}, header map[string]string) (*WDJResponse, error) {
	obs, _ := json.Marshal(obj)
	body := bytes.NewReader(obs)
	rq,err := http.NewRequest("POST", urlLine, body)
	if err != nil {
		return nil, err
	}
	rq.Close = true
	rq.Header.Add("Content-Type","application/json")
	for k, v := range header {
		rq.Header.Add(k, v)
	}
	rsp, err := client.Do(rq)
	if err != nil {
		return nil, err
	}
	bs, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return nil, err
	}
	return &WDJResponse{rsp, string(bs)}, nil
}

func toParamsContent(ps map[string]interface{}) string {
	list := []string{}
	for k, v := range ps {
		var info string
		switch v.(type) {
		case string:
			info = k+"="+v.(string)
		default:
			bs, _ := json.Marshal(v)
			info = k+"="+string(bs)
		}
		list = append(list, info)
	}
	return strings.Join(list, "&")
}

type WDJResponse struct {
	*http.Response
	BodyStr string
}
