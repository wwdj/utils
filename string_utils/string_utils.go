package string_utils

import (
	"crypto/md5"
	"io"
	"fmt"
)

func Md5(s string) string{
	w := md5.New()
	io.WriteString(w,s)
	return fmt.Sprintf("%X",w.Sum(nil))
}
