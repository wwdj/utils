package entity

type TmplConfig struct {
	Mode        string
	Dirs        []string          //初始化文件夹
	TmplFiles   map[string]string //模板文件,生成的文件格式化->文件模板
	NoUpdateCol []string          //不进行更新的字段
}

type DataConfig struct {
	User     string
	Password string
	Host     string
	Port     string
	Database string
	Tables   []string
	Pkg      string
}

type Command struct {
	DataConfig
	Mode string
}
