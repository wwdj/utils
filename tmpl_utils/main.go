package main

import (
	"os"
	"fmt"
	"wdj_utils/tmpl_utils/utils"
	"wdj_utils/tmpl_utils/entity"
	"log"
	"bufio"
	"strings"
	"html/template"
)

//模板方法
var funcMap = template.FuncMap{
	"camel":   utils.CamelString,
	"typeMap": utils.MapToGo,
	"inArray": inArray,
}

func Init(conf entity.TmplConfig) {
	dirs := conf.Dirs
	for i := range dirs {
		item := dirs[i]
		os.MkdirAll(item, os.ModePerm)
	}
}

func format(tConfig entity.TmplConfig, config entity.DataConfig) {
	dataInfo := utils.TableCols(config)
	if config.Tables != nil && len(config.Tables) != 0 {
		tmp := utils.Table{}
		for i := range config.Tables {
			table := config.Tables[i]
			tmp[table] = dataInfo[table]
		}
		dataInfo = tmp
	}

	for table, cols := range dataInfo {
		m := make(map[string]interface{})
		clzName := utils.CamelString(1, table)
		m["pkg"] = config.Pkg
		m["rowClzName"] = table
		m["filedClzName"] = utils.CamelString(0, table)
		m["clzName"] = clzName
		m["cols"] = cols
		m["constNotUpdate"] = tConfig.NoUpdateCol

		for k, v := range tConfig.TmplFiles{
			//k生成的文件名
			//v对应的文件模板
			filename := fmt.Sprintf(k, clzName)

			t, err := template.New("wdj").
				Funcs(funcMap).
				ParseFiles(v)
			if err != nil {
				log.Println(err.Error())
				os.Exit(-1)
			}

			file, _ := os.Create(filename)
			writer := bufio.NewWriter(file)
			t.Execute(writer, m)
			writer.Flush()
			file.Close()
		}
	}
}

func command(args []string) entity.Command {
	config := entity.DataConfig{
		User:     "root",
		Password: "root",
		Host:     "localhost",
		Port:     "3306",
	}
	c := entity.Command{DataConfig: config}
	targs := args[1:]
	for index := range targs {
		item := targs[index]
		bs := []rune(item)
		if bs[0] != '-' {
			panic("参数错误：" + item)
		}
		key := string(bs[1])
		value := string(bs[2:])
		fmt.Printf("key:%s, value:%s \n", string(key), value)
		switch key {
		case "u":
			c.User = value
		case "p":
			c.Password = value
		case "o":
			c.Port = value
		case "d":
			c.Database = value
		case "k":
			c.Pkg = value
		case "t":
			c.Tables = strings.Split(value, ",")
		case "h":
			c.Host = value
		case "m":
			c.Mode = value
		}
	}
	return c
}

func inArray(i string, cols []string) bool {
	for index := range cols {
		item := cols[index]
		if item == i {
			return true
		}
	}
	return false
}

func main() {
	c := command(os.Args)
	fmt.Printf("%+v\n", c)
	tConfig := utils.Configs[c.Mode]
	if tConfig.Mode != c.Mode{
		fmt.Println("错误mode: "+c.Mode)
		os.Exit(-2)
	}
	Init(tConfig)
	format(tConfig, c.DataConfig)
}