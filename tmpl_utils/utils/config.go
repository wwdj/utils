package utils

import "wdj_utils/tmpl_utils/entity"

const DIR = "./"

var Configs = map[string]entity.TmplConfig{
	"beego": {
		Mode: "beego",
		Dirs: []string{
			DIR + "target/controllers/",
			DIR + "target/models/",
		},
		TmplFiles: map[string]string{
			DIR + "target/models/%s.go":      DIR + "tmpl_utils/tmpl_beego/models_tmpl.txt",
			DIR + "target/controllers/%s.go": DIR + "tmpl_utils/tmpl_beego/controllers_tmpl.txt",
		},
		NoUpdateCol: []string{"id", "create_time", "update_time", "status"},
	},
	"java": {
		Mode: "java",
		Dirs: []string{
			DIR + "target/entity/",
			DIR + "target/mapper/",
			DIR + "target/mapper/",
			DIR + "target/service/",
			DIR + "target/service/impl/",
		},
		TmplFiles: map[string]string{
			DIR + "target/entity/%s.java":                  DIR + "tmpl_utils/tmpl/entity_tmpl.txt",
			DIR + "target/mapper/%sMapper.java":            DIR + "tmpl_utils/tmpl/mapper_tmpl.txt",
			DIR + "target/controller/%sController.java":    DIR + "tmpl_utils/tmpl/controller_tmpl.txt",
			DIR + "target/mapper/%sMapper.xml":             DIR + "tmpl_utils/tmpl/mapper_xml_tmpl.txt",
			DIR + "target/service/%sService.java":          DIR + "tmpl_utils/tmpl/service_tmpl.txt",
			DIR + "target/service/impl/%sServiceImpl.java": DIR + "tmpl_utils/tmpl/service_impl_tmpl.txt",
			DIR + "target/model/%sCmd.java":                DIR + "tmpl_utils/tmpl/cmd_tmpl.txt",
		},
		NoUpdateCol: []string{"id", "create_time", "update_time", "status"},
	},
	"php": {
		Mode: "php",
		Dirs: []string{
			DIR + "target/controllers/", DIR + "target/models/",
		},
		TmplFiles: map[string]string{
			DIR + "target/models/%s.go":      DIR + "tmpl_utils/tmpl_beego/models_tmpl.txt",
			DIR + "target/controllers/%s.go": DIR + "tmpl_utils/tmpl_beego/controllers_tmpl.txt",
		},
		NoUpdateCol: []string{"id", "create_time", "update_time", "status"},
	},
}
