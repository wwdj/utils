package utils

import "strings"

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
	"wdj_utils/tmpl_utils/entity"
)
var db *sql.DB

func Init(config entity.DataConfig) {
	if db != nil {
		return
	}

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true",
		config.User, config.Password, config.Host, config.Port, "information_schema")
	_db, err := sql.Open("mysql", dsn);
	if err != nil {
		fmt.Println(err);
		return
	}
	db = _db
}

func TableCols(config entity.DataConfig) Table{
	Init(config)
	q := fmt.Sprintf("SELECT TABLE_NAME,COLUMN_NAME,DATA_TYPE from `COLUMNS` where TABLE_SCHEMA='%s'", config.Database)
	rows, err := db.Query(q)
	if err != nil {
		log.Println(err)
		os.Exit(-2)
	}

	cols := Table{}

	for rows.Next() {
		var table, col, dataType string
		rows.Scan(&table, &col, &dataType)
		cols[table] = append(cols[table], Col{dataType, col})
	}

	return cols
}

type Table map[string][]Col

type Col struct {
	Type string
	Name string
}

func MapToGo(sqlType string) string {
	switch sqlType {
	case "int":
		return "int64"
	case "tinyint":
		return "int"
	case "smallint":
		return "int"
	case "datetime":
		return "time.Time"
	case "bigint":
		return "int64"
	case "varchar":
		return "string"
	case "float":
		return "float"
	case "double":
		return "float64"
	case "decimal":
		return "float64"
	case "text":
		return "string"
	}
	return sqlType
}

func MapToJava(sqlType string) string {
	switch sqlType {
	case "int", "tinyint", "smallint":
		return "int"
	case "datetime":
		return "Timestamp"
	case "bigint":
		return "long"
	case "varchar", "text":
		return "String"
	case "float":
		return "float"
	case "double":
		return "double"
	case "decimal":
		return "BigDecimal"
	}
	return sqlType
}

func CamelString(mode int, s string) string {
	var content []string
	_switch := mode == 1
	for _, value := range strings.Split(s, "_") {
		if _switch {
			chs := []rune(value)
			if 'a' <= chs[0] && chs[0] <= 'z' {
				chs[0] -= 32
				content = append(content, string(chs))
			}
		} else {
			content = append(content, value)
		}
		_switch = true
	}
	return strings.Join(content, "")
}